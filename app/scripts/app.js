(function(angular) {
    'use strict';

    angular.module('tokenizerJsGeneratorApp', [])

    .controller('MainController', [
        '$scope', 
        function ($scope){
            $scope.info = 'Greetings';
        }
    ]);

})(window.angular);