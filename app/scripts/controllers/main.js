'use strict';

/**
 * @ngdoc function
 * @name tokenizerJsGeneratorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tokenizerJsGeneratorApp
 */
angular.module('tokenizerJsGeneratorApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
