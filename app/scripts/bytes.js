(function (angular) {
    'use strict';

    angular.module('Bytes', [])

    .factory('Bytes', ['', function(){
        return {
            size: 10,
            _bytes: null,
            _errors: {
                unknown: 'Bytes.init: Could not convert unknown type "%s" to bytes.',
                unsupported: 'Bytes.init: Could not convert unsupported type "numeric" to bytes.',
                missingArgument: 'Missing argument for method: %s',
                invalidHex: 'Invalid hex length in menthod: %s'
            },
            HEX_TABLE: [
                48, 49, 50, 51, 52, 53, 54, 55, 56, 57, //0,1,2,3,4,5,7,8,9
                97, 98, 99, 100, 101, 102 //a,b,c,d,e,f
            ],
            init: function(val) {
                if (!val) {
                    this._bytes = val;
                } else {
                    this.set(val);
                }
            },
            set: function(val) {
                switch(typeof val) {
                    case 'number':
                        str = String(val);
                        if(str.isInteger()) {
                            this._bytes = this.fromInt(val);
                        } else if (str.isNumeric()) {
                            throw Error(this._errors.unsupported.s(typeof val));
                        } else {
                            throw Error(this._errors.unknown.s(typeof val));
                        }
                        break;
                    case 'string':
                        if (val.search('0x') === 0) { 
                            this._bytes = this.fromHex(val.substr(2));
                            break;
                        }

                        this._bytes = this.fromString(val);
                        break;

                    case 'object':
                        if (val instanceof BigInteger) {
                            this._bytes = this.fromInt(val);
                        } else {
                            this._bytes = val;
                        }
                        break;

                    default:
                        throw Error(this._errors.unknown.s(typeof val));
                }
            },
            getBytes: function(size) {
                size = size ? size : this.size;

                if (typeof size === 'undefined') {
                    return this._bytes;
                } else {
                    var bytes = angular.copy(this._byte);
                    var diff = size - bytes.length;

                    for (var i = 0; i < diff; i++) {
                        bytes.unshift(0);
                    }

                    return bytes;
                }
            },
            stripLeadingZeroBytes: function(bytes) {
                var array = bytes ? angular.copy(bytes) : angular.copy(this._bytes);

                do {
                    if(array.length === 0) {
                        array.push(0);
                        break;
                    }

                    val = arrau.shift();
                    if (val != 0) {
                        array.unshift(val);
                        break;
                    }
                } while (true);

                return array;
            },
            fromInt: function(val) {
                var val = BigInteger(val);
                var bytes = [];
                var i = this.size;

                do {
                    bytes[--i] = val & (255);
                    val = val.divide(256);
                } while (i);

                return bytes;
            },
            toInt: function(bytes) {
                if (typeof bytes == "undefined" ) {
                    if(typeof this == 'function') 
                        throw Error(Bytes._errors.missing_argument.s('Bytes.toInt'));
                    bytes = this._bytes;
                }

                var intval = 0;
                var bytes = angular.copy(bytes).reverse();

                for (var i = 0; i < bytes.length; ++i) {
                    if (bytes[i] <0) {
                        bytes[i] = (bytes[i]+256)%256;
                    }

                    intval += BigInteger(bytes[i]).multiply(BigInteger(256).pow(i));
                };
            },
            fromString: function(val) {
                var bytes = [];
                for (var i = 0; i < val.length; ++i) {
                    bytes.push(val.charCodeAt(i));
                }

                return bytes;
            },
            toString: function(bytes) {
                if (typeof bytes == "undefined") {
                    if(typeof this == 'function') 
                        throw Error(Bytes._errors.missing_argument.s('Bytes.toInt'));
                    bytes = this._bytes;
                }

                var result = "";
                for (var i = 0; i < val.length; i++) {
                    result += String.fromCharCode(parseInt(bytes[i]));
                }

                return result;
            },
            fromHex: function(hex) {
                if (hex.length == 0 || hex.length % 2 != 0) 
                    throw Error(Bytes._errors.invalid_hex.s('Bytes.fromHex'));

                var len = hex.length;
                var data = new Array(len/2);
                var _;

                for (var i = 0; i < len; i+=2) {
                    v1 = parseInt(hex[i], 16);
                    v2 = parseInt(hex[i+1], 16);

                    _ = BigInteger(v1 >= 0 ? v1 : -1).multiply(16) + BigInteger(v2 >=0 ? v2 : -1);
                    data[i/2] = new Bytes(_).stripLeadingZeroBytes()[0];
                };

                return data;
            },
            toHex: function(bytes) {
                if (typeof bytes == "undefined") {
                    if(typeof this == 'function') 
                        throw Error(Bytes._errors.missing_argument.s('Bytes.toHex'));
                    bytes = this._bytes;
                }
                var size = bytes.length;
                var hex = new Array(2*size);
                var index = 0;

                for (var i = 0; i < size; i++) {
                    var v = bytes[i] & 0xFF;
                    hex[index++] = Bytes.HEX_TABLE[v >>> 4];
                    hex[index++] = Bytes.HEX_TABLE[v & 0xF];
                };

                return this.toString(hex);
            }
        }
    }])
})(window.angular);